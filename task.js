/////////////////////////////////////////////////////////////////////////////////////////////
//
// task
//
//    Task class used for standardizing task execution.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var type =  require("type");

var ERROR_INVALID_TASK = "Invalid Task";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Task Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Task(obj) {
    var self = this;

    this.template = null;
    this.description = null;
    this.parameters = {};
    this.status = {
        "progress" : 0,
        "message" : null
    };
    this.ready = null;
    this.node = null;
    this.cancel = false;
    this.error = false;
    this.log = [];
    this.user = null;
    this.created = null;
    this.scheduled = null;
    this.started = null;
    this.ended = null;


    if (obj) {
        //
        // parse template
        //
        if (!type.isObject(obj.template) && obj.template !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'template' should be of type object or null.");
        }
        if (typeof obj.template !== "undefined" && obj.template !== null) {
            self.template = obj.template;
        }

        //
        // parse description
        //
        if (!type.isString(obj.description) && typeof obj.description !== "undefined" && obj.description !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'description' should be of type string or null.");
        }
        if (obj.description != null) {
            self.description = obj.description;
        }

        //
        // parse parameters
        //
        if (!type.isObject(obj.parameters) && obj.parameters !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'parameters' should be of type object or null.");
        }
        if (typeof obj.parameters !== "undefined" && obj.parameters !== null) {
            self.parameters = obj.parameters;
        }

        //
        // parse status
        //
        if (!type.isObject(obj.status)) {
            if (typeof obj.status !== "undefined" && obj.status !== null) {
                throw new Error(ERROR_INVALID_TASK, "Property 'status' should be of type object or null.");
            }
        }
        else {
            if (!type.isNumber(obj.status.progress)) {
                if (typeof obj.status.progress !== "undefined" && obj.status.progress !== null) {
                    throw new Error(ERROR_INVALID_TASK, "Property 'status.progress' should be of type number or null.");
                }
            }
            self.status.progress = obj.status.progress;
            if (type.isString(obj.status.message)) {
                if (typeof obj.status.message !== "undefined" && obj.status.message !== null) {
                    throw new Error(ERROR_INVALID_TASK, "Property 'status.message' should be of type string or null.");
                }
            }
            self.status.message = obj.status.message;
        }

        //
        // parse ready
        //
        if (!type.isBoolean(obj.ready) && typeof obj.ready !== "undefined" && obj.ready !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'ready' should be of type boolean or null.");
        }
        if (typeof obj.ready !== "undefined" && obj.ready != null) {
            self.ready = obj.ready;
        }

        //
        // parse node
        //
        if (!type.isObject(obj.node) && typeof obj.node !== "undefined" && obj.node !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'node' should be of type object or null.");
        }
        if (obj.node != null) {
            self.node = obj.node;
        }

        //
        // parse cancel
        //
        if (!type.isBoolean(obj.cancel) && typeof obj.cancel !== "undefined" && obj.cancel !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'cancel' should be of type boolean or null.");
        }
        if (typeof obj.cancel !== "undefined" && obj.cancel != null) {
            self.cancel = obj.cancel;
        }

        //
        // parse log
        //
        if (!type.isArray(obj.log) && typeof obj.log !== "undefined" && obj.log !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'log' should be of type array or null.");
        }
        if (obj.log != null) {
            self.log = obj.log;
        }

        //
        // parse user
        //
        if (!type.isString(obj.user) && typeof obj.user !== "undefined" && obj.user !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'user' should be of type string or null.");
        }
        if (obj.user != null) {
            self.user = obj.user;
        }

        //
        // parse created
        //
        if (type.isString(obj.created)) {
            obj.created = new Date(obj.created);
        }
        if ((Object.prototype.toString.call(obj.created) !== "[object Date]" || isNaN(obj.created.getTime())) && typeof obj.created !== "undefined" && obj.created !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'created' should be of type string or null.");
        }
        if (typeof obj.created !== "undefined" && obj.created != null) {
            self.created = obj.created;
        }

        //
        // parse scheduled
        //
        if (type.isString(obj.scheduled)) {
            obj.scheduled = new Date(obj.scheduled);
        }
        if ((Object.prototype.toString.call(obj.scheduled) !== "[object Date]" || isNaN(obj.scheduled.getTime())) && typeof obj.scheduled !== "undefined" && obj.scheduled !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'scheduled' should be of type string or null.");
        }
        if (typeof obj.scheduled !== "undefined" && obj.scheduled != null) {
            self.scheduled = obj.scheduled;
        }

        //
        // parse started
        //
        if (type.isString(obj.started)) {
            obj.started = new Date(obj.started);
        }
        if ((Object.prototype.toString.call(obj.started) !== "[object Date]" || isNaN(obj.started.getTime())) && typeof obj.started !== "undefined" && obj.started !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'started' should be of type string or null.");
        }
        if (typeof obj.started !== "undefined" && obj.started != null) {
            self.started = obj.started;
        }

        //
        // parse ended
        //
        if (type.isString(obj.ended)) {
            obj.ended = new Date(obj.ended);
        }
        if ((Object.prototype.toString.call(obj.ended) !== "[object Date]" || isNaN(obj.ended.getTime())) && typeof obj.ended !== "undefined" && obj.ended !== null) {
            throw new Error(ERROR_INVALID_TASK, "Property 'ended' should be of type string or null.");
        }
        if (typeof obj.ended !== "undefined" && obj.ended != null) {
            self.ended = obj.ended;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Task;
module.exports.ERROR_INVALID_TASK = ERROR_INVALID_TASK;